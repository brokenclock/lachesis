function AppViewModel() {
	var self=this;
	self.panels = ['#viewPanel','#editPanel'];
	self.initDb = [];
	self.feedFromDb = [];

	/** Markdownparser **/
	self.mdconvert = new Markdown.getSanitizingConverter().makeHtml;

	/** functions **/
	self.switchOffPanels = function(){
		_.each(self.panels,function(panel){$(panel).hide();});
	};
};
