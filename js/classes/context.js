/** main class */
function context(id,name){
	var self=this;
	self.id=id;
	self.name = ko.observable(name).extend({saveContextInDbTable: self});
}

/** ko.extenders */
ko.extenders.saveContextInDbTable = function(target, option) {
	target.subscribe(function(newValue) {
		lachesisDb.insertOrUpdate('contexts',{id: option.id}, {id:option.id,name: option.name()});
		lachesisDb.commit();
	});
	return target;
};

/** extending main app */
function contextExtendApp(app){
	app.initDb.push(function(){
		if (!lachesisDb.tableExists('contexts')){
			lachesisDb.createTable('contexts',['id','name']);
			lachesisDb.insert('contexts',{id:1,name:'General'});
		}
	});

	app.feedFromDb.push(function(){
		_.each(lachesisDb.queryAll('contexts', {sort: [["name", "DESC"]]}),function(rcontext){
			newContext = new context(rcontext.id,rcontext.name);
			app.contexts.push(newContext);
		});
	});


	app.chosenEditContext = ko.observable().extend({notify: 'always'});
	app.contexts = ko.observableArray();


	app.newContext = function(){
		newContext = new context(getNewIdForTable('contexts'),'');
		app.editContext(newContext);
		app.contexts.push(newContext);
	}

	app.editContext = function(context){
		app.chosenEditContext(context);
		$('#editContext').show();
		window.location.hash="#editContext";
		$('#contextname').focus();
	}

	app.deleteContext = function(context){
		if (window.confirm('Êtes-vous sûr de vouloir supprimer le contexte "'+context.name()+'" ?')) {
			lachesisDb.deleteRows('contexts',{id:context.id});
			lachesisDb.commit();
			app.contexts.remove(context);
		}
	}

	app.getContextName = function(id){
		result = _.find(app.contexts(),function(context){
			if (context.id == id) return context;
		});
		return result.name();
	}

}
