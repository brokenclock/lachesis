/** main class */
function task(id,name,tomate,description,context,deadline,completed,wakeupdate){
	description = typeof description !== 'undefined' ? description : '';
	context = typeof context !== 'undefined' ? context : getFirstIdFromTable('contexts');
	deadline = typeof deadline !== 'undefined' ? deadline : '';
	completed = typeof completed == "boolean" ? completed : false;
	wakeupdate = typeof wakeupdate !== 'undefined' ? wakeupdate : '';
	var self=this;
	self.name = ko.observable(name).extend({saveTaskInDbTable: self});
	self.id = id;
	self.tomate = ko.observable(tomate).extend({saveTaskInDbTable: self,notify: 'always'});
	self.description = ko.observable(description).extend({saveTaskInDbTable: self,notify: 'always'});
	self.context = ko.observable(context).extend({saveTaskInDbTable: self,notify: 'always'});
	self.deadline =ko.observable(deadline).extend({saveTaskInDbTable: self,notify: 'always'});
	self.completed =ko.observable(completed).extend({saveTaskInDbTable: self,notify: 'always'});
	self.wakeupdate =ko.observable(wakeupdate).extend({saveTaskInDbTable: self,notify: 'always'});
};

/** ko.extenders */
ko.extenders.saveTaskInDbTable = function(target, option) {
	target.subscribe(function(newValue) {
		lachesisDb.insertOrUpdate('tasks',{id: option.id}, {name: option.name(),id:option.id,tomate:option.tomate(),description:option.description(),context:option.context(),deadline:option.deadline(),completed:option.completed(),wakeupdate:option.wakeupdate()});
		lachesisDb.commit();
		app.updateAllListPanels(option.id);
	});
	return target;
}

/** extending main app **/
function taskExtendApp(app){
	app.initDb.push(function(){
		if (!lachesisDb.tableExists('tasks'))
			lachesisDb.createTable('tasks',['id','name','tomate','description','context','deadline','completed','wakeupdate']);
		if(!lachesisDb.columnExists('tasks','deadline')) {
			lachesisDb.alterTable('tasks','deadline');
		}
		if(!lachesisDb.columnExists('tasks','completed')) {
			lachesisDb.alterTable('tasks','completed');
		}
		if(!lachesisDb.columnExists('tasks','wakeupdate')) {
			lachesisDb.alterTable('tasks','wakeupdate');
		}
		lachesisDb.commit();
	});

	app.chosenViewTask = ko.observable();
	app.chosenEditTask = ko.observable();
	app.chosenEditTask.extend({notify: 'always'});

	app.viewTask = function(task){
		app.switchOffPanels();
		app.chosenViewTask(task);
		$('#viewPanel').show();
		window.location.hash="#viewPanel";
	};

	app.editTask = function(task){
		app.switchOffPanels();
		app.chosenEditTask(task);
		$('#editPanel').show();
		window.location.hash="#editPanel";
		$('#taskname').focus();
	};

	app.newTask = function(){
		newTask = new task(getNewIdForTable('tasks'));
		app.editTask(newTask);
		app.updateAllListPanels();
	};

	app.deleteTask = function(task){
		if (window.confirm('Are you sure to delete "'+task.name()+'" ?')) {
			lachesisDb.deleteRows('tasks',{id:task.id});
			lachesisDb.commit();
			app.updateAllListPanels();
		}
	}

	app.switchTomate = function(task){
		task.tomate(task.tomate()?false:true);
	}

	app.switchCompleted = function(task){
		date=new Date();
		completionDate = date.toString('yyyyMMddHHmmss');
		task.completed(task.completed()?false:completionDate);
	}
}
