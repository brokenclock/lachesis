/** main class */
function listPanel(id,name,query,order){
	var self=this;
	self.id = id;
	self.name = ko.observable(name).extend({saveListPanelInDbTable: self});
	self.query = ko.observable(query).extend({saveListPanelInDbTable: self});
	self.order = ko.observable(order).extend({saveListPanelInDbTable: self});
	self.tasks = ko.observableArray();
	self.update = function(){
		self.tasks.removeAll();
		tasks = lachesisDb.queryAll('tasks',eval('('+self.query()+')'));
		_.each(tasks,function (rtask){
			newTask =  new task(rtask.id,rtask.name,rtask.tomate,rtask.description,rtask.context,rtask.deadline,rtask.completed,rtask.wakeupdate);
			self.tasks.push(newTask);
		});
		/*if (tasks.length == 0) {
			app.LPanels.remove(self);
			app.LPanels.push(self);
		}*/
	}
	self.update();
}

/** ko.extenders */
ko.extenders.saveListPanelInDbTable = function(target, option) {
	target.subscribe(function(newValue) {
		lachesisDb.insertOrUpdate('listPanels',{id: option.id}, {id:option.id,name: option.name(),query: option.query(),order: parseInt(option.order())});
		lachesisDb.commit();
		option.update();
		app.LPanels.sort(function(a,b){return a.order()-b.order();});
	});
	return target;
};

/** extending main app */
function listpanelExtendApp(app){
	app.initDb.push(function(){
		if (!lachesisDb.tableExists('listPanels')){
			lachesisDb.createTable('listPanels',['id','name','query','order']);
			lachesisDb.insert('listPanels',{id:1,name:'Default',query:'{"query":{}}',order:99});
		}
	});
	app.feedFromDb.push(function(){
		_.each(lachesisDb.queryAll('listPanels', {sort: [["order", "ASC"]]}),function(lpanel){
			newLPanel = new listPanel(lpanel.id,lpanel.name,lpanel.query,lpanel.order);
			app.LPanels.push(newLPanel);
		});
	});

	app.chosenEditLPanel = ko.observable();
	app.chosenEditLPanel.extend({notify: 'always'});
	app.LPanels = ko.observableArray();

	app.newLPanel = function(){
		newPanel = new listPanel(getNewIdForTable('listPanels'),'','{"query":{}}',50);
		app.editLPanel(newPanel);
		app.LPanels.push(newPanel);
	}

	app.deleteLPanel = function(lpanel){
		if (window.confirm('Êtes-vous sûr de vouloir supprimer le panel "'+lpanel.name()+'" ?')) {
			lachesisDb.deleteRows('listPanels',{id:lpanel.id});
			lachesisDb.commit();
			app.LPanels.remove(lpanel);
		}
	}

	app.editLPanel = function(lpanel){
		app.chosenEditLPanel(lpanel);
		$('#editLPanel').show();
		window.location.hash="#editLPanel";
		$('#lpanelname').focus();
	}

	app.updateAllListPanels = function(){
		_.each(app.LPanels(),function(lpanel){
			lpanel.update();
		});
		app.LPanels.sort(function(left,right){
			if (left.tasks().length == 0 && right.tasks().length != 0) return 1;
			if (right.tasks().length == 0) return -1;
			return left.order == right.order ? 0 : (left.order < right.order ? 1 : -1);
		});
	};


}
