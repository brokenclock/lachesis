getFirstIdFromTable = function(table){
	firstItemArray = lachesisDb.queryAll(table,{limit:1,sort:[['ID','ASC']]});
	firstId = firstItemArray[0]?firstItemArray[0].id:1;
	return firstId;
}

getNewIdForTable = function(table){
	lastItemArray = lachesisDb.queryAll(table,{limit:1,sort:[['ID','DESC']]});
	newId = lastItemArray[0]?lastItemArray[0].id+1:1;
	return newId;
}

visible = function(row,enabled){
	enabled = typeof enabled !== 'undefined' ? enabled : '';
	enabled = enabled.split(',');

	if (($.inArray('completed',enabled)==-1) && row.completed)
		return false;

	if (($.inArray('withoutdeadline',enabled)==-1) && !row.deadline){
		return false;
	}

	if (($.inArray('asleep',enabled)==-1) && Date.parse(row.wakeupdate)>Date.today()){
		return false;
	}

	return true;
}
